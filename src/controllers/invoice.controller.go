package controllers

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"restapi-invoice/src/config"
	"restapi-invoice/src/models"
	"github.com/spf13/cast"
	"gorm.io/gorm"
	"math/rand"
)

var db *gorm.DB = config.ConnectDB()

type Item struct {
	ItemId int `json:"item_id"`
	Qty int `json:"qty"`
	Price float64 `json:"price"`
	Amount float64 `json:"amount"`
}
 
type invoiceRequest struct {
	Subject string `json:"subject"`
	IssueDate string `json:"issue_date"`
	DueDate string `json:"due_date"`
	CustomerId int `json:"customer_id"`
	DetailAddress string `json:"detail_address"`
	TotalItem int `json:"total_item"`
	SubTotal float64 `json:"sub_total"`
	Tax float64 `json:"tax"`
	GrandTotal float64 `json:"grand_total"`
	Items []Item `json:"items"`
}

type invoiceResponse struct {
	invoiceRequest
	ID uint `json:"id"`
}

func GetAll(context *gin.Context) {
	var invoices []models.Invoice

	err := db.Preload("Items").Find(&invoices)
	if err.Error != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Error getting data"})
		return
	}

	context.JSON(http.StatusOK, gin.H{
		"status":  "200",
		"message": "Success",
		"data":    invoices,
	})
}

func GetDetail(context *gin.Context) {
	reqParamId := context.Param("id")
	id := cast.ToUint(reqParamId)
	fmt.Print(id)

	invoice := models.Invoice{}

	invoiceById := db.Where("id = ?", id).Preload("Items").First(&invoice)
	fmt.Print(invoiceById)
	if invoiceById.Error != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Invoice not found"})
		return
	}

	context.JSON(http.StatusOK, gin.H{
		"status":  "200",
		"message": "Success",
		"data":    invoice,
	})
}

func Store(context *gin.Context) {
	var data invoiceRequest

	if err := context.ShouldBindJSON(&data); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	invoice := models.Invoice{}

	invoice.InvoiceId = rand.Intn(9999)
	invoice.Subject = data.Subject
	invoice.IssueDate = data.IssueDate
	invoice.DueDate = data.DueDate
	invoice.CustomerId = data.CustomerId
	invoice.DetailAddress = data.DetailAddress
	invoice.TotalItem = data.TotalItem
	invoice.SubTotal = data.SubTotal
	invoice.Tax = data.Tax
	invoice.GrandTotal = data.GrandTotal

	result := db.Create(&invoice)

	if result.Error != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Something went wrong"})
		return
	}

	for _, item := range data.Items {
		invoiceItem := models.InvoiceItem{}

		invoiceItem.InvoiceId = invoice.ID
		invoiceItem.ItemId = item.ItemId
		invoiceItem.Qty = item.Qty
		invoiceItem.Price = item.Price
		invoiceItem.Amount = item.Amount

		db.Create(&invoiceItem)
    }

	var response invoiceResponse

	response.Subject = invoice.Subject
	response.IssueDate = invoice.IssueDate
	response.DueDate = invoice.DueDate
	response.CustomerId = invoice.CustomerId
	response.DetailAddress = invoice.DetailAddress
	response.TotalItem = invoice.TotalItem
	response.SubTotal = invoice.SubTotal
	response.Tax = invoice.Tax
	response.GrandTotal = invoice.GrandTotal

	context.JSON(http.StatusCreated, response)
}

func Update(context *gin.Context) {
	var data invoiceRequest
  
	reqParamId := context.Param("id")
	id := cast.ToUint(reqParamId)

	if err := context.BindJSON(&data); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	invoice := models.Invoice{}

	invoceById := db.Where("id = ?", id).First(&invoice)
	if invoceById.Error != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Invoice not found"})
		return
	}

	invoice.InvoiceId = rand.Intn(9999)
	invoice.Subject = data.Subject
	invoice.IssueDate = data.IssueDate
	invoice.DueDate = data.DueDate
	invoice.CustomerId = data.CustomerId
	invoice.DetailAddress = data.DetailAddress
	invoice.TotalItem = data.TotalItem
	invoice.SubTotal = data.SubTotal
	invoice.Tax = data.Tax
	invoice.GrandTotal = data.GrandTotal

	result := db.Save(&invoice)
	if result.Error != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Something went wrong"})
		return
	}

	invoiceItemDelete := models.InvoiceItem{}
	db.Where("invoice_id = ?", id).Delete(&invoiceItemDelete)
	for _, item := range data.Items {
		invoiceItem := models.InvoiceItem{}

		invoiceItem.InvoiceId = invoice.ID
		invoiceItem.ItemId = item.ItemId
		invoiceItem.Qty = item.Qty
		invoiceItem.Price = item.Price
		invoiceItem.Amount = item.Amount

		db.Create(&invoiceItem)
    }
  
	var response invoiceResponse
	
	response.Subject = invoice.Subject
	response.IssueDate = invoice.IssueDate
	response.DueDate = invoice.DueDate
	response.CustomerId = invoice.CustomerId
	response.DetailAddress = invoice.DetailAddress
	response.TotalItem = invoice.TotalItem
	response.SubTotal = invoice.SubTotal
	response.Tax = invoice.Tax
	response.GrandTotal = invoice.GrandTotal

	context.JSON(http.StatusCreated, response)
}

func Delete(context *gin.Context) {
	  invoice := models.Invoice{}
	  invoiceItem := models.InvoiceItem{}

	  reqParamId := context.Param("id")
	  id := cast.ToUint(reqParamId)
  
	  delete := db.Where("id = ?", id).Delete(&invoice)
	  db.Where("invoice_id = ?", id).Delete(&invoiceItem)
	  fmt.Println(delete)
  
	  context.JSON(http.StatusOK, gin.H{
		  "status":  "200",
		  "message": "Success",
		  "data":    id,
	  })
  
  }