package routes

import (
	"github.com/gin-gonic/gin"
	"restapi-invoice/src/controllers"
)

func Routes() {
	route := gin.Default()

	route.GET("/invoice", controllers.GetAll)
	route.GET("/invoice/:id", controllers.GetDetail)
	route.POST("/invoice", controllers.Store)
	route.PUT("/invoice/:id", controllers.Update)
	route.DELETE("/invoice/:id", controllers.Delete)

	route.Run()
}