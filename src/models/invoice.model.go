package models

import (
	"gorm.io/gorm"
)

type Invoice struct {
	gorm.Model
	ID int
	InvoiceId int
	Subject string
	Status string
	IssueDate string
	DueDate string
	CustomerId int
	DetailAddress string
	TotalItem int
	SubTotal float64
	Tax float64
	GrandTotal float64
	Items []InvoiceItem `gorm:"foreignKey:invoice_id;references:id"`
}