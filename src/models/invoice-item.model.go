package models

import (
	"gorm.io/gorm"
)

type InvoiceItem struct {
	gorm.Model 
	InvoiceId int
	ItemId int
	Qty int
	Price float64
	Amount float64
}